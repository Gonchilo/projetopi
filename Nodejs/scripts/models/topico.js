const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TopicoSchema = new Schema({
    nome:{
        type: String,
        required: true
    }
});

const Topico = module.exports = mongoose.model('Topico', TopicoSchema);