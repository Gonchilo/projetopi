const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ArtigoSchema = new Schema({
    titulo:{
        type: String,
        required: true
    },
    texto: {
        type: String,
        required: true
    },
    data: {
        type: Date,
        required: true,
        default: Date.now()
    },
    photoPath: {
        type: String,
        required: true,
        default: '../caminho/foto.jpg'
    },
    aceite: {
        type: String,
        required: true,
        default: "Pendente"
    },
    user_id: {
        type: String,
        required: true
    },
    subTopico_id: {
        type: String,
        required: true
    },
    topico_id: {
        type: String,
        required: true
    }
});

const Artigo = module.exports = mongoose.model('Artigo', ArtigoSchema);