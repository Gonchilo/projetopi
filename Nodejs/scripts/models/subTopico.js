const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SubTopicoSchema = new Schema({
    nome:{
        type: String,
        required: true
    },
    topico_id: {
        type: String,
        required: true
    }
});

const SubTopico = module.exports = mongoose.model('SubTopico', SubTopicoSchema);