const express = require('express');
const router = express.Router();
const User = require('../models/user');
const config = require('../../config/database');
const bcrypt = require('bcryptjs');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, '../Sustentabilidade/src/assets/images/')
    },
    filename: (req, file, callBack) => {
        callBack(null, `${file.originalname}`)
    }
});
  
const upload = multer({ storage: storage });

// http://localhost:8080/api/users/register
// Registar utilizador
router.post('/register', (req, res) => {
    var newUser = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        admin: req.body.admin,
        temporaryToken: jwt.sign({ username: req.body.username, email: req.body.email}, config.secret, {expiresIn: '24h'})
    });

    User.findOne({ email: newUser.email }).select().exec((err, user) => {
        if(err) { res.json({ sucess: false, message: err});}

        if(!user){
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if(err){ res.json({ sucess: false, message: err}); }
                    else {
                        newUser.password = hash;
                        newUser.save((err, doc) => {
                            if(err) { res.json({ sucess: false, message: err}); }
                            else {
                                res.json({ sucess: true, message: "Utilizador criado", user: newUser});
                            }
                        })
                    }
                });
            });
        } else {
            res.json({ sucess: false, message: 'Email already exist!' });
        }
    });
});

// http://localhost:8080/api/users/login
// Iniciar sessao do utilizador
router.post('/login', (req, res, next) => {
    const emailB = req.body.email;
    const passwordB = req.body.password;

    User.findOne({ email: emailB }).select().exec((err, user) => {
        if(err) { res.json({ sucess: false, message: err })}
        else{
            if(!user){
                res.json({ sucess: false, message: 'Email não existe'});
            } else {
                bcrypt.compare(passwordB, user.password, (err, result) => {
                    if(result){
                        const token = jwt.sign(user.toJSON(), config.secret, {
                            expiresIn: 604800    
                        });
                        res.json({
                            sucess: true,
                            token: 'Bearer ' + token,
                            user: user
                        });
                    } else {
                        res.json( { sucess: false, message: 'Password errada'});
                    }
                });
            }
        }
    });
});

// http://localhost:8080/api/users
// Retorna todos os utlizadores
router.get('/', (req, res) => {
    User.find((err, users) => {
        if(err) { res.json({ sucess: false, message: "Erro ao receber os Utilizadores"})}
        else {
            res.send(users);
        }
    });
});

// http://localhost:8080/api/users/:id
// Retorna o utilizador pelo seu id
router.get('/:id', (req, res) => {
    user_id = req.params.id;

    User.findById(user_id,(err, user) =>{
        if(err){res.json({ sucess: false, message: "Id não encontrado"})}
        else {
            res.send(user);
        }
    });
});

// http://localhost:8080/api/users/logged/logged
// Retorna o utilizador loggado
router.get('/logged/logged', passport.authenticate('jwt', { session: false}), (req, res, next) => {
    res.json(req.user);
});


// http://localhost:8080/api/users
// Modifica o utilizador
router.put('/:id', (req, res) => {
    var putUser = {};
    putUser.username = req.body.username,
    putUser.email = req.body.email,

    User.updateOne({ _id: req.params.id}, putUser, (err, user) => {
        if(err){res.json({ sucess: false, message: "Id não encontrado"});}
        else {res.json({putUser});}
    });
});

// http://localhost:8080/api/users
// Apaga o utilizador pelo seu id
router.delete('/:id', (req,res) => {
    User.findByIdAndDelete(req.params.id, (err, user) => {
        if(err){res.json({ sucess: false, message: "Id não encontrado"});}
        else {res.json({ sucess: true, message: "Utilizador apagador", user: user})}
    });
});

// http://localhost:8080/api/users/logged_user/user_type
// Retorna o tipo de user
router.get('/logged_user/user_type', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    User.findOne({ email: req.user.email }).select().exec((err, user) => {
        if(err) { throw err; }
        if(!user) {
            res.json({ success: false, message: 'Id de utilizador não existe' });
        } else {
            if(user.admin){
                res.json({ success: true, message: "Admin" });
            } else {
                res.json( {success: true, message: "User"})
            }
        }
    });
});

// http://localhost:8080/api/users/file
router.post('/file', upload.single('file'), (req, res, next) => {
    const file = req.file;
    var random_filename = generate_token(32) + '.' + file.originalname.split('.')[1];
    fs.rename(file.destination + file.originalname, file.destination + random_filename, function (err) {
        if(err) res.json( {sucess: false, message: err});
    });
    if (!file){
        const error =new Error('No File');
        error.httpStatusCode = 400;
        return next(error);
    }

    res.json({ sucess: true, message: 'Foto foi carregada com sucesso', originalname: random_filename });
});

// => localhost:8081/api/changePhoto
// Muda o caminho da foto de um utilizador
router.post('/changePhoto', passport.authenticate('jwt', { session: false }), (req, res) => {
    User.findOne({ email: req.user.email }).select().exec((err, user) => {
        if(err) { throw err; }

        if(!user) {
            res.json({ sucess: false, message: 'Email não existe' });
        } else {
            const split_string = user.photoPath.split("/");
            
            if(split_string[5] != "default_profile.png"){
                fs.unlink('../Sustentabilidade/src/assets/images/' + split_string[5], (err) => {
                  if (err) throw err;
                  console.log('successfully deleted');
                });
            }
            
            user.photoPath = req.body.new_photoPath;
    
            user.save((err, doc) => {
                if(!err) { res.json({ sucess: true, message: 'Foto alterada' }); }
            });
        }

        
    });
});

// => localhost:8080/api/users/editProfile
// Altera variáveis do perfil do utilizador
router.put('/edit/profile', passport.authenticate('jwt', { session: false }), (req, res) => {
    User.findOne({ email: req.user.email }).select().exec((err, user) => {
        if(err) { throw err; }

        if(!user) {
            res.json({ sucess: false, message: 'Email não existe' });
        } else {
            if(req.body.new_username != undefined){ user.username = req.body.new_username; }
            if(req.body.new_email != undefined){ user.email = req.body.new_email; }

            user.save((err, doc) => {
                if(!err) { res.json({ sucess: true, message: 'Perfil editado' }); }
            });
        }
    });
});

module.exports = router;