const express = require('express');
const router = express.Router();
const Topico = require('../models/topico');
const config = require('../../config/database');

// http://localhost:8080/api/topico
// Retorna todos os topicos
router.get('/', (req, res) => {
    Topico.find((err, topico) => {
        if(!err){ res.json(topico);}
        else { res.json({ sucess: false, message: err});}
    });
});

// http://localhost:8080/api/topico
// Retorna um topico por id
router.get('/:id', (req, res) => {
    Topico.findById( req.params.id, (err, topico) => {
        if(!err){ res.json(topico);}
        else { res.json({ sucess: false, message: err});}
    });
});

// http://localhost:8080/api/topico
// Cria um topico
router.post('/', (req, res) => {
    newTopico = new Topico({
        nome: req.body.nome
    });

    newTopico.save((err, topico) => {
        if(!err){ res.json(topico);}
        else{ res.json({ sucess: false, message: err});}
    });
});

// http://localhost:8080/api/topico
// Apaga um topico pelo seu id
router.delete('/:id', (req, res) => {
    Topico.findByIdAndDelete( req.params.id, (err, topico) => {
        if(!err){ res.json(topico);}
        else { res.json({ sucess: false, message: err});}
    });
});

// http://localhost:8080/api/topico/id/:nome
// Retorna um topico _id pelo seu nome
router.get('/id/:nome', (req, res) => {
    Topico.find({ nome: req.params.nome }).select().exec((err, topico) => {
        if(err) ({ sucess: false, message: err});
        if(topico){
            res.json(topico);
        } else {
            res.json({ sucess: false, message: "Nome do topico não existe"});
        }
    });
});

// http://localhost:8080/api/topico/nomes/todos
// Retorna o nome de todos os topicos 
router.get('/nomes/todos', (req, res) => {
    Topico.find((err, topicos) => {
        if(err) res.json( {sucess: false, message: err});
        else {
            let nomes = [];
            topicos.forEach(topico => {
                nomes.push(topico.nome);
            });
            res.json(nomes);
        }
    });
});

module.exports = router;