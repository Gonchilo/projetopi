const express = require('express');
const router = express.Router();
const Topico = require('../models/topico');
const SubTopico = require('../models/subTopico');
const config = require('../../config/database');

// http://localhost:8080/api/subtopico
// Retorna todos os subtopicos
router.get('/', (req, res) => {
    SubTopico.find((err, subtopico) => {
        if(!err){ res.json(subtopico);}
        else { res.json({ sucess: false, message: err});}
    });
});

// http://localhost:8080/api/subtopico
// Retorna um subtopico por id
router.get('/:id', (req, res) => {
    SubTopico.findById( req.params.id, (err, subtopico) => {
        if(!err){ res.json(subtopico);}
        else { res.json({ sucess: false, message: err});}
    });
});

// http://localhost:8080/api/subtopico
// Cria um subtopico
router.post('/', (req, res) => {
    newSubTopico = new SubTopico({
        nome: req.body.nome
    });

    Topico.findById( {_id : req.body.topico_id}, (err, topico) => {
        if(!err){
            newSubTopico.topico_id = req.body.topico_id
            newSubTopico.save((err, subtopico) => {
                if(!err){ res.json(subtopico);}
                else{ res.json({ sucess: false, message: err});}
            });
        } else {
            res.json({ sucess: false, message: 'Topico id nao corresponde a um topico'})
        }
    });    
});

// http://localhost:8080/api/subtopico
// Apaga um subtopico pelo seu id
router.delete('/:id', (req, res) => {
    SubTopico.findByIdAndDelete( req.params.id, (err, subtopico) => {
        if(!err){ res.json(subtopico);}
        else { res.json({ sucess: false, message: err});}
    });
});

// http://localhost:8080/api/subtopico/id/:nome
// Retorna um subtopico _id pelo seu nome
router.get('/id/:nome', (req, res) => {
    SubTopico.find({ nome: req.params.nome }).select().exec((err, subtopico) => {
        if(err) ({ sucess: false, message: err});
        if(subtopico){
            res.json(subtopico);
        } else {
            res.json({ sucess: false, message: "Nome do subtopico não existe"});
        }
    });
});

// http://localhost:8080/api/subtopico/nomes/todos
// Retorna o nome de todos os subtopicos 
router.get('/nomes/todos', (req, res) => {
    SubTopico.find((err, subtopicos) => {
        if(err) res.json( {sucess: false, message: err});
        else {
            let nomes = [];
            subtopicos.forEach(subtopico => {
                nomes.push(subtopico.nome);
            });
            res.json(nomes);
        }
    });
});

// http://localhost:8080/api/subtopico/nomes/:nome
// Retorna o nome de todos os subtopicos consoante o nome do topico
router.get('/nomes/:nome', (req, res) => {
    Topico.find({ nome: req.params.nome }).select().exec((err, topico) => {
        if(err) res.json( {sucess: false, message: err});
        else {
            SubTopico.find((err, subtopicos) => {
                if(err) res.json( {sucess: false, message: err});
                else {
                    let nomes = [];
                    subtopicos.forEach(subtopico => {
                        if(topico[0]._id.toString() == subtopico.topico_id.toString()){
                            nomes.push(subtopico.nome);
                        }
                    });
                    res.json(nomes);
                }
            });
        }
    });
});

module.exports = router;