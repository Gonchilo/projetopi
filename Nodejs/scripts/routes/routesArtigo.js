const express = require('express');
const router = express.Router();
const Artigo = require('../models/artigo');
const User = require('../models/user');
const Topico = require('../models/topico');
const SubTopico = require('../models/subTopico');
const config = require('../../config/database');
const passport = require('passport');

// http://localhost:8080/api/artigo
// Criar um artigo
router.post('/', (req, res) => {
    console.log(req.body);
    newArtigo = new Artigo({
        titulo: req.body.titulo,
        texto: req.body.texto,
        user_id: req.body.user_id
    });
    Topico.findById({ _id: req.body.topico_id }, (err, topico) => {
        if(!err){
            newArtigo.topico_id = req.body.topico_id;
            SubTopico.findById({ _id: req.body.subTopico_id}, (err, subtopico) => {
            if(!err){
                newArtigo.subTopico_id = req.body.subTopico_id;
                newArtigo.save((err, artigo) => {
                    if(!err){ res.json(artigo);}
                    else { res.json( {sucess: false, message: err} ); }
                });
            } else {
                res.json({ sucess: false, message: 'SubTopico id nao corresponde a um sub topico'})
            }
            });
        } else {
            res.json({ sucess: false, message: 'Topico id nao corresponde a um topico'})
        }
    })
    
});

// http://localhost:8080/api/artigo
// Retorna todos os artigos
router.get('/', (req, res) => {
    Artigo.find((err, artigo) => {
        if(!err){ res.json(artigo);}
        else{res.json({ sucess: false, message: err})}
    });
});

// http://localhost:8080/api/artigo/:id
// Retorna o artigo pelo id
router.get('/:id', (req, res) => {
    Artigo.findById( req.params.id, (err, artigo) => {
        if(!err){ res.json(artigo);}
        else{res.json({ sucess: false, message: err})}
    });
});

// http://localhost:8080/api/artigo/:id
// Retorna o artigo pelo id
router.put('/:id', (req, res) => {
    var putArtigo = {};
    putArtigo.texto = req.body.texto;
    putArtigo.aceite = false;

    Artigo.findOneAndUpdate( req.params.id, putArtigo, (err, artigo) => {
        if(!err){ res.json(artigo);}
        else{res.json({ sucess: false, message: err})}
    });
});

// http://localhost:8080/api/artigo/:id/aprove
// Retorna todos os artigos do 
router.get('/:id/aprove', (req, res) => {            
    Artigo.findOneAndUpdate(req.params.id, { aceite: "Aceite" }, (err, artigo) => {
        if(err){ res.json(err);}
        else {
            res.json( {sucess: true, message: "Artigo aceite"});
        }
    });

});

// http://localhost:8080/api/artigo/:id/reject
// Retorna todos os artigos do 
router.get('/:id/reject', (req, res) => {            
    Artigo.findOneAndUpdate(req.params.id, { aceite: "Rejeitado" }, (err, artigo) => {
        if(err){ res.json(err);}
        else {
            res.json( {sucess: true, message: "Artigo Rejeitado"});
        }
    });

});

module.exports = router;