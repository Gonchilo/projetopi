const config = require('./config/database');
const express = require("express");
const app = express();
const path = require('path');
const passport = require('passport');
const bodyParser = require("body-parser");
const { mongoose } = require('./db.js');
const cors = require('cors');

const routesUser = require('./scripts/routes/routesUser.js');
const routesArtigo = require('./scripts/routes/routesArtigo.js');
const routesTopico = require('./scripts/routes/routesTopico.js');
const routesSubTopico = require('./scripts/routes/routesSubTopico.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

//CORS Middleware
app.use(cors());
app.use(function (req, res, next) {
    //Enabling CORS 
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);

app.use('/api/users', routesUser);
app.use('/api/artigo', routesArtigo);
app.use('/api/topico', routesTopico);
app.use('/api/subtopico', routesSubTopico);


//Index Route
app.get("/", function (req, res) {
    res.sendFile("index.html");
});

//ERROR Page
app.get('*', function(req, res){
    res.send('Erro, URL inválido.');
});

var server = app.listen(8080, function () {
    var host = server.address().address === "::" ? "localhost" :
    server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
});

module.exports = app;