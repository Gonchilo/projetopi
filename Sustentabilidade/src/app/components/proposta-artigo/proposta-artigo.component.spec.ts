import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropostaArtigoComponent } from './proposta-artigo.component';

describe('PropostaArtigoComponent', () => {
  let component: PropostaArtigoComponent;
  let fixture: ComponentFixture<PropostaArtigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropostaArtigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropostaArtigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
