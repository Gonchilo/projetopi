import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { UserService } from '../../services/userService/user.service';
import { ArtigoService } from '../../services/artigoService/artigo.service';
import { SubTopicoService } from '../../services/subTopicoService/subtopico.service';
import { TopicoService } from '../../services/topicoService/topico.service';
import { User } from '../../services/userService/user.model'
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-proposta-artigo',
  templateUrl: './proposta-artigo.component.html',
  styleUrls: ['./proposta-artigo.component.css'],
  providers: [UserService, ArtigoService, SubTopicoService, TopicoService]
})
export class PropostaArtigoComponent implements OnInit {
  user: User;
  titulo: string;
  texto: string;
  subTopico_id: string;
  user_id: any;
  topico: string;
  topicos: string[];
  subtopico: string;
  subtopicos: string[];
   

  constructor(
    private userService: UserService,
    private artigoService: ArtigoService,
    private subTopicoService: SubTopicoService,
    private topicoService: TopicoService,
    private router: Router
  ) { }

  ngOnInit() {
    this.resetForm();
    this.getDBTopicos();
    
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.artigoService.selectedArtigo = {
      _id: "",
      photoPath: "",
      aceite: false,
      titulo: "",
      texto: "",
      user_id: "",
      subTopico_id: "",
      topico_id: "",
      data: ""
    }
  }

  onSubmit(form: NgForm) {
    const artigo = {
      titulo: this.titulo,
      texto: this.texto
    }

    this.getSelectedTopico();

    this.userService.getLoggedUser().subscribe(res => {
      this.user_id = res._id;
      form.value.user_id = res._id;
      this.topicoService.getTopicoIdByName(this.topico).subscribe(ressss => {
        form.value.topico_id = ressss[0]._id
        this.subTopicoService.getSubTopicoIdByName(this.subtopico).subscribe(resss => {
          form.value.subTopico_id = resss[0]._id;
          this.artigoService.postArtigos(form.value).subscribe(ress => {
            this.resetForm(form);
              Swal.fire({
                icon: 'success',
                text: "Artigo proposto para revisão"
              });
              this.router.navigate(['/']);
          });
        });
      });
    });    
  }

  getSelectedTopico(){
    var opcao;
    this.topicos.forEach(function(err, i ,a) {
      var radiobutton = document.getElementById("topico"+i)  as HTMLInputElement
      if(radiobutton.checked){
        opcao = (a[i].toString());
      }    
    });
    this.topico = opcao;
    this.getDBSubTopicosByTopicoNome(opcao);
    //console.log(opcao);
  }

  
 
  getDBSubTopicosByTopicoNome(opcao){
    this.subTopicoService.getSubTopicosByTopicoNome(opcao).subscribe(res => {
      this.subtopicos = res;
    });    
  }

  getDBTopicos(){
    this.topicoService.getTopicosNomes().subscribe(res => {
      this.topicos = res;
    });
  }

  onFileChanged(event) {
    const file = event.target.files[0];
    console.log(file);
  }
}
