import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../../services/userService/user.service'
import {Router} from '@angular/router';
import { Body } from '@angular/http/src/body';
import Swal from 'sweetalert2';

@Component({ 
  selector: 'app-login',
  templateUrl: './login.component.html',
  providers: [UserService],
  styleUrls: ['./login.component.css'] 
})
export class LoginComponent implements OnInit {
  email:String;
  password:String;

  constructor(
    private userService: UserService, 
    private router: Router
  ) { }

  ngOnInit(){
    this.resetForm();
  }

  onSubmit(form: NgForm){
    console.log(form.value);
    this.userService.login(form.value).subscribe(res => {
      this.resetForm(form);
      console.log(res["_body"]);
      if(JSON.parse(res["_body"]).sucess){
        this.userService.storeUserData(JSON.parse(res["_body"]).token, JSON.parse(res["_body"]).user);
        this.router.navigate(['/']);
      } else {
        Swal.fire({
          icon: 'error',
          text: JSON.parse(res["_body"]).message
        });
      }
    });
  }

  resetForm(form?: NgForm) {
    if(form) form.reset();
    this.userService.selectedUser = {
      _id: "",
      username: "",
      email: "",
      password: "",
      admin: "",
      photoPath: "",
    }
  }

}
