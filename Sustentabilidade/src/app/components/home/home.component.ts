import { UserService } from './../../services/userService/user.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  isAdmin: boolean = false

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userType();
  }

  userType(){
    this.userService.getUserType().subscribe(userType => {
      if(userType.message == 'Admin'){
        this.isAdmin = true;
      }
    });
  }

}
