import { Component, OnInit } from '@angular/core';
import { Artigo } from '../../services/artigoService/artigo.model';
import { UserService } from '../../services/userService/user.service';
import { ArtigoService } from '../../services/artigoService/artigo.service';
import { SubTopicoService } from '../../services/subTopicoService/subtopico.service';
import { TopicoService } from '../../services/topicoService/topico.service';
import { User } from '../../services/userService/user.model'
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-artigo',
  templateUrl: './artigo.component.html',
  styleUrls: ['./artigo.component.css'],
  providers: [UserService, ArtigoService, SubTopicoService, TopicoService]
})
export class ArtigoComponent implements OnInit {
  artigo: Artigo;
  user: any;
  data: String;
  topico: String;
  subtopico: String;
  texto: String;

  constructor(
    private userService: UserService,
    private artigoService: ArtigoService,
    private subTopicoService: SubTopicoService,
    private topicoService: TopicoService,
    private router: Router
  ) { }

  ngOnInit(): void {
    var artigoId = window.location.href.split('/')[4];

    this.artigoService.getArtigo(artigoId).subscribe(artigo => {
      this.artigo = artigo;
      console.log("Artigo "+this.artigo.user_id);
      this.userService.getUserById(this.artigo.user_id.toString()).subscribe(userr => {
        this.user = userr;
        console.log("user "+this.user);
      });
      this.topicoService.getTopico(this.artigo.topico_id).subscribe(topicoo => {
        this.topico = topicoo.nome;
        console.log("topico "+this.topico);
      });
      this.subTopicoService.getSubTopico(this.artigo.subTopico_id).subscribe(subtopicoo => {
        this.subtopico = subtopicoo.nome;
        console.log("subtopico "+this.subtopico);
      })
      this.data = moment(artigo.data).locale('pt').format('LL');
      this.texto = artigo.texto;
    });
    
  }

}
