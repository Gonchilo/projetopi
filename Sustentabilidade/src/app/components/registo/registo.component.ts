import { Component, OnInit } from '@angular/core';
import { NgForm, FormsModule  } from '@angular/forms';
import {Http, Headers} from '@angular/http';
import { UserService } from '../../services/userService/user.service';
import { User } from '../../services/userService/user.model';
import { Body } from '@angular/http/src/body';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registo',
  templateUrl: './registo.component.html',
  providers: [UserService],
  styleUrls: ['./registo.component.css', '../footer/footer.component.css']
})
export class RegistoComponent implements OnInit {
  username: String;
  email: String;
  password: String;

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if(form)
    form.reset();
    this.userService.selectedUser = {
      _id: "",
      username: "",
      email: "",
      password: "",
      admin: "",
      photoPath: "",
    }
  }

  onSubmit(form: NgForm) {
    const user = {
      username: this.username,
      email: this.email,
      password: this.password
    }

    console.log(form.value);

    this.userService.register(form.value).subscribe(res => {
      if(res.sucess){
        this.resetForm(form);
        this.router.navigate(['/login']);
      } else {
        this.router.navigate(['/register']);
      }
    });
  }
}
