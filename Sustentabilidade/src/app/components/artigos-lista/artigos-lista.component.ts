import { Component, OnInit } from '@angular/core';
import { Artigo } from '../../services/artigoService/artigo.model';
import { UserService } from '../../services/userService/user.service';
import { ArtigoService } from '../../services/artigoService/artigo.service';
import { SubTopicoService } from '../../services/subTopicoService/subtopico.service';
import { TopicoService } from '../../services/topicoService/topico.service';
import { User } from '../../services/userService/user.model'
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-artigos-lista',
  templateUrl: './artigos-lista.component.html',
  styleUrls: ['./artigos-lista.component.css']
})
export class ArtigosListaComponent implements OnInit {
  listaArtigos;
  colunas;
  user: any;
  data: String;
  topico: String;
  subtopico: String;

  constructor(private userService: UserService,
    private artigoService: ArtigoService,
    private subTopicoService: SubTopicoService,
    private topicoService: TopicoService,
    private router: Router) { }

  ngOnInit(): void {
    this.colunas = ["Titulo", "Texto", "Topico", "Subtopico", "Nome do Autor", "Data"];
    var listaArtigos = [];

    this.artigoService.getArtigos().subscribe(artigos => {
      artigos.forEach(artigo => {
        if(artigo.aceite == "Aceite"){
          listaArtigos.push(artigo);
          this.listaArtigos = listaArtigos;
          this.userService.getUserById(artigo.user_id.toString()).subscribe(userr => {
            this.user = userr;
          });
          this.topicoService.getTopico(artigo.topico_id).subscribe(topicoo => {
            this.topico = topicoo.nome;
          });
          this.subTopicoService.getSubTopico(artigo.subTopico_id).subscribe(subtopicoo => {
            this.subtopico = subtopicoo.nome;
          })
          this.data = moment(artigo.data).locale('pt').format('LL');
        }
        });    
    });

    
  }

}
