import Swal from 'sweetalert2';
import { Artigo } from './../../services/artigoService/artigo.model';
import { ArtigoService } from './../../services/artigoService/artigo.service';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';


@Component({
  selector: 'app-admin-accept-artigo',
  templateUrl: './admin-accept-artigo.component.html',
  styleUrls: ['./admin-accept-artigo.component.css']
})
export class AdminAcceptArtigoComponent implements OnInit {
colunas;

formattedDate: string;

listaArtigos;

  constructor(private artigoService: ArtigoService) { }

  ngOnInit() {
    this.colunas = ["Titulo", "Data de submissão", "Ação"];

    var listaArtigos = [];

    this.artigoService.getArtigos().subscribe(artigos => {
      artigos.forEach(artigo => {
        if(artigo.aceite == "Pendente"){
          listaArtigos.push(artigo);
          this.listaArtigos = listaArtigos;
          console.log(listaArtigos);
        }
      });
    });
  }

  onAccept(id){
    this.artigoService.aproveArtigo(id).subscribe(res => {
      if(res.sucess){
        Swal.fire({
          icon: 'success',
          text: 'Artigo aceite!'
        }).then(function(){
          window.location.reload();
        });
      }
    });
  }

  onReject(id){
    this.artigoService.rejectArtigo(id).subscribe(res => {
      
        Swal.fire({
          icon: 'success',
          text: 'Artigo Rejeitado com sucesso!'
        }).then(function(){
          window.location.reload();
        });
      
    });
  }

}
