import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAcceptArtigoComponent } from './admin-accept-artigo.component';

describe('AdminAcceptArtigoComponent', () => {
  let component: AdminAcceptArtigoComponent;
  let fixture: ComponentFixture<AdminAcceptArtigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAcceptArtigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAcceptArtigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
