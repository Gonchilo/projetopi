import { SubTopicoService } from './../../services/subTopicoService/subtopico.service';
import { TopicoService } from './../../services/topicoService/topico.service';
import { ArtigoService } from './../../services/artigoService/artigo.service';
import { UserService } from './../../services/userService/user.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-estatisticas',
  templateUrl: './estatisticas.component.html',
  styleUrls: ['./estatisticas.component.css']
})
export class EstatisticasComponent implements OnInit {
  totalUsers: number;
  totalArtigos: number;
  totalTopicos: number;
  totalSubTopicos: number;
  totalAceites: number;
  totalPendentes: number;
  totalRejeitados: number;

  constructor(private userService: UserService, private artigoService: ArtigoService, private topicoService: TopicoService, private subtopicos: SubTopicoService) { }

  ngOnInit() {
    var totalUsers = 0;
    var totalArtigos = 0;
    var totalTopicos = 0;
    var totalSubTopicos = 0;
    var totalAceites = 0;
    var totalPendentes = 0;
    var totalRejeitados = 0;

    this.userService.getUsers().subscribe(res => {
      res.forEach(user => {
        totalUsers++;
      });
      this.totalUsers = totalUsers;
    });
    this.artigoService.getArtigos().subscribe(res => {
      res.forEach(artigo => {
        totalArtigos++;
        if(artigo.aceite == "Pendente"){
          totalPendentes++;         
        } else if (artigo.aceite == "Aceite"){
          totalAceites++;         
        } else {
          totalRejeitados++;         
        }
      });
      this.totalPendentes = totalPendentes;
      this.totalArtigos = totalArtigos;
      this.totalAceites = totalAceites;
      this.totalRejeitados = totalRejeitados;
    });
    this.topicoService.getTopicos().subscribe(res => {
      res.forEach(res => {
        totalTopicos++;
      });
      this.totalTopicos = totalTopicos;
    });
    this.subtopicos.getSubTopicos().subscribe(res => {
      res.forEach(res => {
        totalSubTopicos++;
      });
      this.totalSubTopicos = totalSubTopicos;
    });
    this.artigoService.getArtigos().subscribe(res => {

    });
  }

}
