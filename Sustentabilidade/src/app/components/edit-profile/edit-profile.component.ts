import { User } from './../../services/userService/user.model';
import { UserService } from './../../services/userService/user.service';
import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup } from '@angular/forms';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css'],
  providers: [UserService]
})
export class EditProfileComponent implements OnInit {
  username: string;
  email: string;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.userService.getLoggedUser().subscribe(res => {
      this.username = res.username;
      this.email = res.email;
    })
  }

  onSubmit(form: NgForm){
    this.userService.editProfile(this.username, this.email).subscribe(res => {
      console.log(res);
      
      if(res.sucess){
        Swal.fire({
          icon: 'success',
          text: 'Perfil editado com sucesso!'
        });
        this.router.navigate(['/profile']);
        
      }else{
        Swal.fire({
          icon: 'error',
          text: res.message
        });
      }
    });
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
    this.userService.selectedUser = {
      _id: "",
      username: "",
      email: "",
      password: "",
      admin: "",
      photoPath:""
    }
  }

}
