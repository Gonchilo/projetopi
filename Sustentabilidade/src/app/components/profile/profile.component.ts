import { User } from './../../services/userService/user.model';
import { Router } from '@angular/router';
import { UserService } from './../../services/userService/user.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public user: User;
  userId: string;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userService.getLoggedUser().subscribe(res => {
      this.user = res;
      this.userId = res._id
    });
  }

  onSubmit(form: NgForm){
    this.userService.deleteUser(this.userId).subscribe(res => {
      this.userService.storeUserData(JSON.parse(res["_body"]).token, JSON.parse(res["_body"]).user);
      this.userService.logout();
      this.refresh();
    });
  }

  resetForm(form?: NgForm) {
    if (form)
      form.reset();
  }

  refresh(): void {
    window.location.reload();
  }
}
