export class User {
    _id: string;
    username: string;
    email: string;
    password: string;
    admin: string;
    photoPath: string;
}