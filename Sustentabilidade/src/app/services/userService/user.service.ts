import { Injectable } from '@angular/core';
import { Http, Headers, HttpModule } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tokenNotExpired } from 'angular2-jwt';

import { User } from './user.model';
import { apiUrl } from '../../../apiUrl.loader';

@Injectable()
export class UserService {
  authToken: any;
  user: any;
  selectedUser: User;
  users: User[]
  readonly baseURL = apiUrl;
  readonly URL = this.baseURL + '/users';

  constructor(private http: Http) { }

  getUsers(){
    return this.http.get(this.URL).map(res => res.json());
  }

  getUserById(id){
    return this.http.get(this.URL + '/' + id).map(res => res.json());
  }

  deleteUser(_id: string) {
    return this.http.delete(this.baseURL + '/users' + `/${_id}`);
  }

  register(user: User){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.URL + '/register', user, {headers: headers}).map(res => res.json());
  }

  login(user: User){
    return this.http.post(this.URL + '/login', user);
  }

  storeUserData(token, user){
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  logout(){
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  loadToken(){
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  loggedIn(){
    return tokenNotExpired('id_token');
  }

  getUserType(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type','application/json');
    return this.http.get(this.baseURL + '/users/logged_user/user_type', {headers: headers})
      .map(res => res.json());
  }

  getLoggedUser(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type','application/json');
    return this.http.get(this.baseURL + '/users/logged/logged', {headers: headers}).map(res => 
    res.json());
  }

  editProfile(username: string, email: string){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('Content-Type','application/json');
    return this.http.put(this.URL + '/edit/profile', {new_username: username, new_email: email}, {headers: headers})
      .map(res => res.json());
  }
 
}
