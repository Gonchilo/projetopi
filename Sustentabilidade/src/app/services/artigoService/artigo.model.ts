export class Artigo {
    _id: string;
    photoPath: string;
    aceite: boolean;
    titulo: string;
    texto: string;
    user_id: string;
    subTopico_id: string;  
    topico_id: string; 
    data: string;
}