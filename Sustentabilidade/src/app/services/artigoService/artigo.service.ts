import { Injectable } from '@angular/core';
import { Http, Headers, HttpModule } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tokenNotExpired } from 'angular2-jwt';

import { Artigo } from './artigo.model';
import { apiUrl } from '../../../apiUrl.loader';

@Injectable()
export class ArtigoService {
  authToken: any;
  selectedArtigo: Artigo;
  artigos: Artigo[];
  readonly baseURL = apiUrl+ '/artigo';

  constructor(private http: Http) { }

  getArtigos(){
    return this.http.get(this.baseURL).map(res => res.json());
  }

  postArtigos(artigo: Artigo){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseURL, artigo, {headers: headers}).map(res => res.json());
  }

  aproveArtigo(id){
    return this.http.get(this.baseURL + '/' + id +'/aprove').map(res => res.json());
  }

  rejectArtigo(id){
    return this.http.get(this.baseURL + '/' + id +'/reject').map(res => res.json());
  }

  getArtigo(id){
    return this.http.get(this.baseURL + '/' + id).map(res => res.json());
  }
}
