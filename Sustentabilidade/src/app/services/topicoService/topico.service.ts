import { Injectable } from '@angular/core';
import { Http, Headers, HttpModule } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tokenNotExpired } from 'angular2-jwt';

import { Topico } from './topico.model';
import { apiUrl } from '../../../apiUrl.loader';

@Injectable()
export class TopicoService {
  authToken: any;
  selectedTopico: Topico;
  topico: Topico[];
  readonly baseURL = apiUrl +'/topico';

  constructor(private http: Http) { }

  getTopicos(){
    return this.http.get(this.baseURL).map(res => res.json());
  }

  getTopico(id){
    return this.http.get(this.baseURL + '/' + id).map(res => res.json());
  }

  getTituloIdByName(name){
    return this.http.get(this.baseURL + '/name/' + name).map(res => res.json());
  }

  getTopicosNomes(){
    return this.http.get(this.baseURL+'/nomes/todos').map(res => res.json());
  }

  getTopicoIdByName(nome){
    return this.http.get(this.baseURL + '/id/' + nome).map(res => res.json());
  }

}
