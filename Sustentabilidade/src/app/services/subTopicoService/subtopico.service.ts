import { Injectable } from '@angular/core';
import { Http, Headers, HttpModule } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { tokenNotExpired } from 'angular2-jwt';

import { SubTopico } from './subtopico.model';
import { apiUrl } from '../../../apiUrl.loader';

@Injectable()
export class SubTopicoService {
  authToken: any;
  selectedSubTopico: SubTopico;
  subTopicos: SubTopico[];
  readonly baseURL = apiUrl + '/subtopico';


  constructor(private http: Http) { }

  getSubTopicos(){
    return this.http.get(this.baseURL).map(res => res.json());
  }

  getSubTopico(id){
    return this.http.get(this.baseURL + '/' + id).map(res => res.json());
  }

  postSubTopico(subtopico: SubTopico){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.baseURL, subtopico, {headers: headers}).map(res => res.json());
  }

  getSubTopicosNomes(){
    return this.http.get(this.baseURL+'/nomes/todos').map(res => res.json());
  }

  getSubTopicosByTopicoNome(nome){
    return this.http.get(this.baseURL+'/nomes/'+nome).map(res => res.json());
  }

  getSubTopicoIdByName(nome){
    //http://localhost:8080/api/subtopico/id/:nome
    return this.http.get(this.baseURL + '/id/' + nome).map(res => res.json());
  }
}
