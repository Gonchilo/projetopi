import { TestBed } from '@angular/core/testing';

import { SubtituloService } from './subtitulo.service';

describe('SubtituloService', () => {
  let service: SubtituloService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubtituloService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
