import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserService } from '../services/userService/user.service';
import { Observable } from 'rxjs';

@Injectable()
export class UserGuard implements CanActivate {
    constructor (private userService: UserService, private router: Router){
    }

    canActivate() : boolean | Observable<boolean> | Promise<boolean>{
        if(this.userService.loggedIn()) {
            return true;
        } else {
            this.router.navigate(['/']);
            return false;
        }
    }
}