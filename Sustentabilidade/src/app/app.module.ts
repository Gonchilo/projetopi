

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { LoginComponent } from './components/login/login.component';
import { RegistoComponent } from './components/registo/registo.component';
import { UserService } from './services/userService/user.service';
import { FooterComponent } from './components/footer/footer.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UserGuard } from './guards/user.guard';
import { AdminGuard } from './guards/admin.guard';
import { PropostaArtigoComponent } from './components/proposta-artigo/proposta-artigo.component';
import { ArtigoService } from './services/artigoService/artigo.service';
import { TopicoService } from './services/topicoService/topico.service';
import { SubTopicoService } from './services/subTopicoService/subtopico.service';
import { ArtigoComponent } from './components/artigo/artigo.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { AdminAcceptArtigoComponent } from './components/admin-accept-artigo/admin-accept-artigo.component';
import { HomeComponent } from './components/home/home.component';
import { SobreComponent } from './components/sobre/sobre.component';
import { EstatisticasComponent } from './components/estatisticas/estatisticas.component';
import { ArtigosListaComponent } from './components/artigos-lista/artigos-lista.component';



const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'naver', component: NavBarComponent},
  {path: 'login', component: LoginComponent},
  {path: 'registo', component: RegistoComponent},
  {path: 'editarPerfil', component: EditProfileComponent, canActivate:[UserGuard]},
  {path: 'profile', component: ProfileComponent, canActivate:[UserGuard]},
  {path: 'propostaArtigo', component: PropostaArtigoComponent, canActivate:[UserGuard]},
  {path: 'artigo/:id', component: ArtigoComponent},
  {path: 'adminPropostas', component: AdminAcceptArtigoComponent},
  {path: 'sobre', component: SobreComponent},
  {path: 'estatisticas', component: EstatisticasComponent, canActivate:[AdminGuard]},
  {path: 'artigos', component: ArtigosListaComponent},


]

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LoginComponent,
    RegistoComponent,
    FooterComponent,
    EditProfileComponent,
    ProfileComponent,
    PropostaArtigoComponent,
    ArtigoComponent, AdminAcceptArtigoComponent, HomeComponent, SobreComponent, EstatisticasComponent, ArtigosListaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    BrowserAnimationsModule
  ],
  providers: [
    UserService,
    UserGuard,
    AdminGuard,
    ArtigoService,
    TopicoService,
    SubTopicoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
